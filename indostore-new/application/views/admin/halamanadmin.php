<div class="container-fluid">
	<div class="row">
		<div class="col">
			<?php if ($this->session->flashdata('flash')): ?>    
		        <div class="row">
		            <div class="col-md-8 mx-auto text-center">
		                <div class="alert alert-success alert-dismissible fade show" role="alert">
		                    produk <strong> <?= $this->session->flashdata('flash');  ?></strong>
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                    </button>
		                </div>
		            </div>
		        </div>
		    <?php endif ?>
		    <?php if ($this->session->flashdata('success')): ?>    
		        <div class="row">
		            <div class="col-md-8 mx-auto text-center">
		                <div class="alert alert-success alert-dismissible fade show" role="alert">
		                     <strong> <?= $this->session->flashdata('success').' '.$pengguna->namadepan  ?></strong>
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                    </button>
		                </div>
		            </div>
		        </div>
		    <?php endif ?>
		</div>
	</div>
</div>

<div class="container-fluid"> 
<div class="row text-center"> 
		<div class="col mt-3"> 
			<div class="row mt-3 mb-3">
				<div class="col">
					<a href="<?= site_url() ?>Admin/lihat_pesanan" class="btn btn-primary btn-block">lihat pesanan</a>
				</div>
			</div>
				<h1>Upload Barang</h1>
				<div class="row d-flex justify-content-center"> 
						<div class="col"> 
								<table class="table">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Kode Barang</th>
								      <th scope="col">Kategori</th>
									 	<th scope="col">Nama Barang</th>
									 	<th scope="col">Deskripsi</th>
								      <th scope="col">Gambar</th>
								      <th scope="col">Harga</th>
								      <th scope="col">opsi</th>
								    </tr>
								  </thead>
								  <tbody>

								  	<?php
								  	$no = 1;
								  	foreach ($barang as $brg): ?>
								    
								    <tr>
									    <th scope="row"><?=  $no++; ?></th>
									    <td><?= $brg->kode  ?></td>
									    <td><?= $brg->kategori  ?></td>
									    <td><?= $brg->namabarang  ?></td>
									    <td><?= $brg->deskripsi  ?></td>
									    <td><img src="<?= base_url()?>/assets/image/<?=$brg->gambar ?>" width='50px'></td>
								        <td><?= $brg->harga  ?></td>
								        <td>
								        	<a href="<?= base_url() ?>Admin/hapusproduk/<?= $brg->id_produk ?>" class="btn btn-danger">hapus</a>
								        	<a href="<?= base_url() ?>Admin/editproduk/<?= $brg->id_produk ?>" class="btn btn-primary">Edit</a>
								        </td>
									</tr>
								  	<?php endforeach ?>
								  </tbody>
								</table>
						</div>	
				</div>
				<div class="row"> 
						<a href="<?= site_url() ?>Admin/tambah_barang" class="btn btn-primary ml-2 text-left">Tambahkan Barang</a>
				</div>	
		</div>	
</div>		
</div>