<div class="container-fluid mt-3 text-center">
	<div class="row">
		<div class="col">
			<h4>Lihat Pesanan</h4>
		</div>
	</div>
</div>

<div class="container-fluid"> 
<div class="row text-center"> 
		<div class="col mt-3"> 
				<div class="row d-flex justify-content-center"> 
						<div class="col"> 
								<table class="table">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Kode Pemesanan</th>
								      <th scope="col">Tanggal Pemesanan</th>
									 	<th scope="col">Nama Pemesan</th>
									 	<th scope="col">Nama Barang</th>
								      <th scope="col">Gambar</th>
								      <th scope="col">harga</th>
								      <th scope="col">verifikasi</th>
								      <th scope="col">opsi</th>
								    </tr>
								  </thead>
								  <tbody>

								  	<?php
								  	$no = 1;
								  	foreach ($pesanan as $pes): ?>
								    
								    <tr>
								    	<form action="" method="post">
									    	<input type="hidden" name="id_pesanan" value="2">
									    	<input type="hidden" name="nomor_order" value="<?= $pes->nomor_order?>">
									    	<input type="hidden" name="tanggal_pesan" value="<?= $pes->tanggal_pesan?>">
									    	<input type="hidden" name="userid" value="<?= $pes->userid?>">
									    	<input type="hidden" name="produkid" value="<?= $pes->produkid?>">
									    	<input type="hidden" name="alamatid" value="<?= $pes->alamatid?>">
									    	<input type="hidden" name="verifikasi" value='1'>
										    <th scope="row"><?=  $no++; ?></th>
										    <td><?= $pes->nomor_order  ?></td>
										    <td><?= $pes->tanggal_pesan  ?></td>
										    <td><?= $pes->namadepan.' '.$pes->namabelakang  ?></td>
										    <td><?= $pes->namabarang  ?></td>
										    <td><img src="<?= base_url()?>/assets/image/<?=$pes->gambar ?>" width='50px'></td>
									        <td><?= $pes->harga  ?></td>
									        <td><?= $pes->verifikasi  ?></td>
									        <td>
									        	<a href="<?= base_url() ?>Admin/verifikasi/<?= $pes->id_pesanan ?>" class="btn btn-primary" onclick="return confirm('Apakah anda yakin ingin melakukan verifikasi ?')">Verifikasi</a>
									        </td>
								    	</form>
									</tr>
								  	<?php endforeach ?>
								  </tbody>
								</table>
						</div>	
				</div>
				<div class="container">
					<div class="row"> 
							<a href="<?= site_url() ?>Admin">kembali</a>
					</div>	
				</div>
		</div>	
</div>		
</div>