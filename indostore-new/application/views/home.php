
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo base_url(); ?>/assets/carosel/1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo base_url(); ?>/assets/carosel/2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo base_url(); ?>/assets/carosel/3.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <div class="container-fluid justify-content-center  text-center">    
    <div class="row mt-5">
      <div class="col">
        <div class="row ">
          <div class="col-4 text-center">
            <img src="<?php echo base_url(); ?>/assets/home-page/1.jpg" style="width: 330px ; height: 330px">
            <h4 class="mt-2 text-primary">MOUNTAINEERING</h4>
            <a href="<?php echo base_url(); ?>Home/katalog" class="text-muted"><h4>Discover More</h4></a>
            <div class="border-bottom mt-3"></div>
          </div>
          <div class="col-4  text-center">
            <img src="<?php echo base_url(); ?>/assets/home-page/2.jpg" style="width: 330px ; height: 330px">
            <h4 class="mt-2 text-primary">1989</h4>
            <a href="<?php echo base_url(); ?>Home/katalog1" class="text-muted"><h4>Discover More</h4></a>
            <div class="border-bottom mt-3"></div>
          </div>
          <div class="col-4  text-center">
            <img src="<?php echo base_url(); ?>/assets/home-page/3.jpg" style="width: 330px ; height: 330px">
            <h4 class="mt-2 text-primary">RIDING</h4>
            <a href="<?php echo base_url(); ?>Home/katalog2" class="text-muted"><h4>Discover More</h4></a>
            <div class="border-bottom mt-3"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="container-fluid mt-5">
  <div class="row">
    <div class="col-6">
      <h2>Lorem Ipsum Dolor Sit Amet</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo pellentesque lorem non vulputate. Duis ac congue sem. Quisque ultrices, turpis a semper rutrum, arcu velit finibus nisi, et interdum est magna eu urna. Etiam ullamcorper mollis volutpat. Duis a mauris et nisi faucibus iaculis et ac nulla. Morbi et feugiat dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec cursus ligula ex, vitae interdum nisl blandit in. Pellentesque ante metus, gravida congue vehicula in, scelerisque sit amet quam. Proin id bibendum mauris. Fusce rhoncus massa eget nisi imperdiet, sed accumsan arcu auctor. Cras a eros dui. Sed vel dapibus arcu, consectetur pretium dolor. Aliquam erat eros, porttitor eget elit a, pharetra lobortis turpis. Mauris imperdiet purus dui, eget fringilla odio scelerisque sit amet. Cras id varius dui, et posuere urna.
      </p>
    </div>

    <div class="col-6">
      <h2>Lorem Ipsum Dolor Sit Amet</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo pellentesque lorem non vulputate. Duis ac congue sem. Quisque ultrices, turpis a semper rutrum, arcu velit finibus nisi, et interdum est magna eu urna. Etiam ullamcorper mollis volutpat. Duis a mauris et nisi faucibus iaculis et ac nulla. Morbi et feugiat dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec cursus ligula ex, vitae interdum nisl blandit in. Pellentesque ante metus, gravida congue vehicula in, scelerisque sit amet quam. Proin id bibendum mauris. Fusce rhoncus massa eget nisi imperdiet, sed accumsan arcu auctor. Cras a eros dui. Sed vel dapibus arcu, consectetur pretium dolor. Aliquam erat eros, porttitor eget elit a, pharetra lobortis turpis. Mauris imperdiet purus dui, eget fringilla odio scelerisque sit amet. Cras id varius dui, et posuere urna.
      </p>
    </div>
  </div>
</div>